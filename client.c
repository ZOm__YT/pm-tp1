#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme client */

int main(int argc, char *argv[]) {

  /* je passe en paramètre l'adresse de la socket du serveur (IP et
     numéro de port) et un numéro de port à donner à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    exit(1);
  }

  char* sIp = argv[1];
  char* sPortServer = argv[2];
  char* sPortClient = argv[3];

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_DGRAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Client : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Client : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  
  /* Etape 2 : Nommer la socket du client */
  
  /* Etape 3 : Désigner la socket du serveur */
  
   struct sockaddr_in sAdr;
   sAdr.sin_family = AF_INET;
   sAdr.sin_addr.s_addr = inet_addr(sIp);
   sAdr.sin_port = htons((short) atoi(sPortServer));

   printf("Htons : %s -> %hu\n", sPortServer, sAdr.sin_port);

   // Base envoi message chaîne de caractères

   char msg[100];
   printf("Entrez votre message : ");
   scanf("%s", msg);

   // Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)
  
   int bytesSent = sendto(ds, msg, 100, 0, (struct sockaddr*) &sAdr, sizeof(sAdr));
   if(bytesSent < 0){
      perror("Erreur lors de l'envoi du message !");
      exit(1);
   }

   int res;
   int bytesReceived = -1;
   socklen_t lgAdr = sizeof(sAdr);
   while(bytesReceived < 0){
      bytesReceived = recvfrom(ds, &res, sizeof(int), 0, (struct sockaddr*) &sAdr, &lgAdr);
   }

   printf("Entier reçu : %d\n", res);


   char* checkAdr = inet_ntoa(sAdr.sin_addr);
   int port = ntohs(sAdr.sin_port);

   printf("Adresse récupérée : %s:%d\n", checkAdr, port);

   close(ds);  



  /* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  
  printf("Client : je termine\n");
  return 0;
}
