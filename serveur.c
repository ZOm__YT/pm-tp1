#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme serveur */

int main(int argc, char *argv[]) {

  /* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }

   char* sPort = argv[1];

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_DGRAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Serveur : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Serveur : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  
  /* Etape 2 : Nommer la socket du seveur */
 
   struct sockaddr_in sAdr;
   sAdr.sin_family = AF_INET;
   sAdr.sin_addr.s_addr = INADDR_ANY;
   sAdr.sin_port = htons((short) atoi(sPort));

   if(bind(ds, (struct sockaddr*) &sAdr, sizeof(sAdr)) < 0){
      perror("Erreur de nommage de la socket");
      exit(1);
   }

   // Base réception chaîne de caractères
   char msg[100];
   int bytesRecv = -1;
   socklen_t lgAdr = sizeof(sAdr);
   while(bytesRecv < 0){
      bytesRecv = recvfrom(ds, msg, 100, 0, (struct sockaddr*) &sAdr, &lgAdr);
   }

   sendto(ds, &bytesRecv, sizeof(int), 0, (struct sockaddr*) &sAdr, lgAdr);
   printf("Message reçu : %s\n", msg);
   printf("Envoi des %d octets reçus\n", bytesRecv);

  

   close(ds);

  /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
  
  /* Etape 5 : envoyer un message au serveur (voir sujet pour plus de détails)*/
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  
  printf("Serveur : je termine\n");
  return 0;
}
